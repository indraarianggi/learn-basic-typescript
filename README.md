# Learn TypeScript

## How to show the result of each file?

1. Clone this repository

    ```
    git clone https://gitlab.com/indraarianggi/learn-basic-typescript.git
    ```

2. Install dependencies

    ```
    yarn install
    ```

3. Execute this command in ternimal to compile the typescript code/file

    ```
    yarn ts
    ```

4. Execute this command to show the result of code in each file.

    ```
    node ./build/<filename>.js
    ```

    Or you can use `nodemon` too:

    ```
    nodemon ./build/<filename>.js
    ```

## Resources

1. [TypeScript Dasar playlist from Nusendra.com youtube channel](https://www.youtube.com/playlist?list=PLnQvfeVegcJbjCnML6FdusK-rl-oDRMXJ)
