import Asus from "./Asus";
import MacBook from "./MacBook";

const asus = new Asus<string>("Zenbook", true, true);
console.log(asus);

const macbook = new MacBook<number>(2017, false, true);
console.log(macbook);
macbook.a();
macbook.b();
