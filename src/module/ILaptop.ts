/**
 * Interface => blueprint untuk class laptop
 */

interface ILaptop<T> {
    name: string;
    type: T;
    withNumeric: boolean;
    withTouchButton: boolean;
}

export default ILaptop;
