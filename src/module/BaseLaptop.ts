/**
 * Base Class (abstact class) untuk laptop
 */

import ILaptop from "./ILaptop";
import * as KeyBoard from "./Keyboard";

abstract class BaseLaptop<T> implements ILaptop<T> {
    name: string;
    type: T;
    withNumeric: boolean;
    withTouchButton: boolean;

    constructor(
        name: string,
        type: T,
        withNumeric: boolean,
        withTouchButton: boolean
    ) {
        this.name = name;
        this.type = type;
        this.withNumeric = withNumeric;
        this.withTouchButton = withTouchButton;
    }

    a(): void {
        console.log(KeyBoard.a());
    }

    b(): void {
        console.log(KeyBoard.b());
    }
}

export default BaseLaptop;
