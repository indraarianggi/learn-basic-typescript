/**
 * INTERFACE => kontrak / blueprint dari sebuah class
 */

interface Laptop {
    name: string;

    on(): void;

    off(): void;
}

class Asus implements Laptop {
    name: string;
    isGaming: boolean;

    constructor(name: string, isGaming: boolean = false) {
        this.name = name;
        this.isGaming = isGaming;
    }

    on(): void {
        console.log("Start the Asus laptop");
    }

    off(): void {
        console.log("Shutting down the Asus laptop");
    }
}

class Macbook implements Laptop {
    name: string;
    keyboardLigth: boolean;

    constructor(name: string, keyboardLigth: boolean = true) {
        this.name = name;
        this.keyboardLigth = keyboardLigth;
    }

    on(): void {
        console.log("Start the Macbook laptop");
    }

    off(): void {
        console.log("Shutting down the Macbook laptop");
    }
}

let asus = new Asus("Asus ROG", true);
console.log(asus.name);
console.log(asus.on());
console.log(asus.off());

let macbook = new Macbook("Macbook Pro");
console.log(macbook.name);
console.log(macbook.keyboardLigth);
console.log(macbook.on());
