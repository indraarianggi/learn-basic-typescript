/**
 * Class
 *
 * public       => bisa diakses di semua class / dari luar class
 * protected    => hanya bisa diakses dari class tersebut dan class turunannya
 * private      => hanya bisa diakses dari class itu sendiri
 *
 * static property / method => bisa diakses langsung dari class tanpa instansiasi object class terlebih dahulu
 */

export class User {
    public name: string;

    constructor(name: string, public age: number) {
        this.name = name;
    }

    setName(value: string): void {
        this.name = value;
    }

    getName(): string {
        return this.name;
    }
}

let user = new User("Indra", 25);
console.log(user);

/**
 * Inheritance
 */
class Admin extends User {
    read: boolean = true;
    write: boolean = true;
    phone: string;

    private _email: string = "";

    // static property
    static role: string = "admin";

    // static method
    static getRole(): string {
        return "admin";
    }

    constructor(name: string, age: number, phone: string) {
        super(name, age); // call constructor of parent class (User)
        this.phone = phone;
    }

    getAccess(): { read: boolean; write: boolean } {
        return {
            read: this.read,
            write: this.write,
        };
    }

    // setter
    set email(value: string) {
        this._email = value;
    }

    // getter
    get email(): string {
        return this._email;
    }
}

let admin = new Admin("Admin 1", 0, "081111111111");
console.log(admin);

admin.email = "indra@gmail.com";
console.log(admin);
console.log(admin.email);

// access static property & method
console.log(Admin.role);
console.log(Admin.getRole());
