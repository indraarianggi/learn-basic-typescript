/**
 * STRING
 */
let myName: string = "Indra Arianggi";

// myName = 10;         // => error
// myName = true;       // => error
myName = "Suryaatmaja";

/**
 * NUMBER (integer, float)
 */
let myAge: number;

// myAge = 'Indra';     // => error
// myAge = false;       // => error
myAge = 25;

/**
 * BOOLEAN (true / false)
 */
let isOn: boolean;
isOn = true;
isOn = false;

/**
 * ANY
 */
let x: any = "Hello";

x = 10;
x = true;
x = ["a", "b", "c"];
x = [1, 2, 3];
x = { name: "indra", age: 25 };

/**
 * UNION TYPE
 */
let phone: string | number;

phone = 6281111111111;
phone = "081111111111";
