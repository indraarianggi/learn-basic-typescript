/**
 * ARRAY
 */
let array = [1, 2, 3]; // => array of number

// array = ["a", "b", "c"];     // => error, because it's array of string

let array2: string[];
array2 = ["a", "b", "c"];

let array3: any[];
array3 = [1, "a", true, {}];

/**
 * TUPLES => array yang isinya bisa berbagai macam tipe data, tetapi itemnya terbatas
 */
let biodata: [string, number];

biodata = ["Indra", 24];
// biodata = [24, "Indra"];     // => error, karena posisinya terbalik antara item yg string dan number
