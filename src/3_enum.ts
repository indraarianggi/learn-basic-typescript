/**
 * ENUM => menyimpan sekumpulan data konstan
 */

/**
 * NUMERIC ENUM
 */
enum month {
    JAN = 1,
    FEB,
    MAR = 100,
    APR,
    MAY,
}

console.log(month);
console.log(month.JAN);
console.log(month.APR);
console.log(month[1]);

/**
 * STRING ENUM
 */
enum month2 {
    JAN = "January",
    FEB = "February",
    MAR = "March",
    APR = "April",
    MAY = "May the force be with you",
}

console.log(month2);
console.log(month2.MAY);
