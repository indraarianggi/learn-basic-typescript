/**
 * GENERIC => tipe data yang dinamis
 */

function getData<T>(value: T) {
    return value;
}

console.log(getData<string>("Indra").length);
// console.log(getData<number>(123).length);     // => error

// With arrow funtion
const getData2 = <T>(value: T) => {
    return value;
};

console.log(getData2<boolean>(true));
