/**
 * FUNCTION
 */

/**
 * Function that returns a value
 */
function getName(): string {
    return "Indra Arianggi";
}
console.log(getName());

function getAge(): number {
    return 25;
}
console.log(getAge());

/**
 * Function that NOT returns a value
 */
function greeting(): void {
    console.log("Hello World!");
}
greeting();

/**
 * Function with argument(s)
 */
function multiply(val1: number, val2: number): number {
    return val1 * val2;
}

const result = multiply(10, 3);
console.log(result);

/**
 * Function as type
 */
type Tambah = (a: number, b: number) => number;

const Add: Tambah = (val1: number, val2: number): number => {
    return val1 + val2;
};

/**
 * Default parameter
 */
const fullName = (first: string, last: string = "Suryaatmaja"): string => {
    return `${first} ${last}`;
};

console.log(fullName("Indra"));
console.log(fullName("Indra", "Arianggi"));

/**
 * Optional parameter
 */
const printName = (first: string, last?: string): string => {
    return `${first} ${last ?? ""}`;
};
console.log(printName("Indra"));
console.log(printName("Indra", "Arianggi"));
