/**
 * OBJECT
 */
type TUser = {
    name: string;
    age: number;
};

let user: TUser = {
    name: "Indra",
    age: 25,
};

user = {
    name: "Arianggi",
    age: 20,
};
