/**
 * GENERIC CLASS
 */

class List<T> {
    private data: T[];

    constructor(elements: T[]) {
        this.data = elements;
    }

    add(element: T): void {
        this.data.push(element);
    }

    addMultiple(elements: T[]): void {
        this.data.push(...elements);
    }

    getAll(): T[] {
        return this.data;
    }
}

const numbers = new List<number>([1, 2, 3]);
numbers.add(4);
numbers.addMultiple([5, 6, 7]);
console.log(numbers.getAll());

const mixed = new List<number | string>([1, "b"]);
mixed.add("c");
mixed.add(4);
mixed.addMultiple([5, "f", 7, 8, "1"]);
console.log(mixed.getAll());
